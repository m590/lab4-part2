const express = require('express');
const schoolCtrl = require('../controller/school.ctrl');

const router = express.Router();

router.route("/students")
    .get(schoolCtrl.getAllStudents);
router.route("/students/:studentId")
    .get(schoolCtrl.getOneStudent);
router.route("/students/:studentId/courses")
    .get(schoolCtrl.getAllCourses);
router.route("/students/:studentId/courses/:courseId")
    .get(schoolCtrl.getOneCourse);

module.exports = router;