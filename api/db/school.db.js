const mongoose = require('mongoose');
require('./student.model');

const url = process.env.db_url + process.env.db_name_school;

console.log('url', url);

mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, function () {
    console.log('school db connected');
});
