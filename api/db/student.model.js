const mongoose = require('mongoose');

const courseModel = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    courseId: Number
});

const studentModel = mongoose.Schema({
    studentId: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    gpa: Number,
    courses: [courseModel]
});

mongoose.model("Student", studentModel, "students");