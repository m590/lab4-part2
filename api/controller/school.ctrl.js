const mongoose = require('mongoose');
const Student = mongoose.model("Student");

module.exports.getAllStudents = function (req, res) {
    let count = 1;
    if (req.query && req.query.count) {
        count = parseInt(req.query.count);
    }
    let offset = 0;
    if (req.query && req.query.offset) {
        offset = parseInt(req.query.offset);
    }

    Student.find().skip(offset).limit(count).exec(function (err, students) {
        res.status(200).json(students);
    });

}

module.exports.getOneStudent = function (req, res) {
    const studentId = req.params.studentId;
    const query = {
        studentId: studentId
    };
    Student.find(query).exec(function (err, student) {
        res.status(200).json(student);
    });
}


module.exports.getAllCourses = function (req, res) {
    const studentId = req.params.studentId;
    const query = {
        studentId: studentId
    };
    Student.findOne(query).exec(function (err, student) {
        const courses = student.courses;
        res.status(200).json(courses);
    });
};

module.exports.getOneCourse = function (req, res) {
    const studentId = parseInt(req.params.studentId);
    const courseId = parseInt(req.params.courseId);

    const query = {
        studentId: studentId,
        courses: {
            $elemMatch: {courseId: courseId}
        }
    }

    Student.findOne(query).exec(function (err, student) {
        const course = student.courses.filter(c => c.courseId == courseId);
        res.status(200).json(course);
    });
};