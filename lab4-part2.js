const config = require('dotenv').config();
const express = require('express');
require('./api/db/school.db');
const router = require('./api/router/router');

const app = express();
app.use("/api", router);


const server = app.listen(process.env.port, function () {
    console.log('School server started at', server.address().port);
});